## 20 July 2019 - v1.1.0 ##  
* Limit post on homepage

## 23 May 2019 - v1.0.9 ##  
* Adjust homepage blog and list
* Tidy up style

## 23 May 2019 - v1.0.8 ##  
* Add read more button
* Add favicon
* Tidy up style

## 19 May 2019 - v1.0.7 ##  
* Adjust blog and destination page
* Tidy up style

## 17 May 2019 - v1.0.6 ##  
* Update Checker

## 17 May 2019 - v1.0.5 ##  
* Styling components
* Change tag

## 15 May 2019 - v1.0.4 ##  
* Styling components
* Minify JS
* Add alt image
* Change tag

## 15 May 2019 - v1.0.3 ##  
* Styling heading and header
* Add function for SEO support
* Change tag

## 12 May 2019 - v1.0.2 ##  
* Styling font and footer
* Remove unused code and tag

## 11 May 2019 - v1.0.1 ##  
* Main components adjustment

## 01 May 2019 - v1.0.0 ##  
* Initial Files
