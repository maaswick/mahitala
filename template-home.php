<?php

/**
 * Template Name: Homepage
 */

get_header();

?>

<div id="content" class="homepage">
  <section class="part-mainslider marginbot-40">
    <div class="section slider-main">
      <?php
        $images = get_field('home_slider');
        if( $images ): ?>

          <div id="slider-main" class="owl-carousel">
            <?php foreach( $images as $image ): ?>
              <div class="owl-slide">
                <div class="image-slide" style="background-image: url('<?php echo $image['url']; ?>')"></div>
                <div class="container">
                  <?php echo $image['caption']; ?>
                </div>
              </div>
            <?php endforeach; ?>
          </div><!-- end .slider-main -->

      <?php endif; ?>
    </div>
  </section>

  <?php if( get_field('section_about_us') ): ?>
    <section class="part-about marginbot-40 padding-tbxlarge" data-aos="fade-up" data-aos-duration="1000">
      <div class="container">
        <div class="col-md-6 col-sm-6">
          <div class="heading-section margin-bmedium text-left">
            <?php
              if( have_rows('tabout') ):
                while ( have_rows('tabout') ): the_row();
                $taboutheading = get_sub_field('ht_about_heading');
                $taboutsubheading = get_sub_field('ht_about_subheading');
            ?>

            <h1><?php echo $taboutheading ?></h1>
            <p><?php echo $taboutsubheading ?></p>

            <?php
              endwhile;
              else :
              // no rows found
              endif;
            ?>
          </div><!-- end .heading-section -->
          <div class="about-post">
            <?php the_field('about_content'); ?>
          </div><!-- end .introduce-post -->
        </div>
        <div class="col-md-6 col-sm-6">
          <ul class="image-intro">
            <li>
              <span class="unimage">
                <?php $imgf = get_field('about_first_image');
                  if( $imgf ):
                ?>
                  <img src="<?php echo $imgf['url']; ?>" alt="<?php echo $imgf['alt']; ?>" class="img-responsive">
                <?php endif; ?>
              </span>
            </li>
            <li>
              <span class="unimage">
                <?php $imgs = get_field('about_second_image');
                  if( $imgs ):
                ?>
                  <img src="<?php echo $imgs['url']; ?>" alt="<?php echo $imgs['alt']; ?>" class="img-responsive">
                <?php endif; ?>
              </span>
            </li>
          </ul>
        </div>
      </div><!-- end .container -->
    </section>
  <?php else: ?>
    <!-- undisplay about us -->
  <?php endif; ?>

  <?php if( get_field('section_destination') ): ?>
    <section class="part-destination marginbot-40 padding-tbxlarge" data-aos="fade-up" data-aos-duration="1000">
      <div class="container">
        <div class="uniheading text-center marginbot-30">
          <?php
            if( have_rows('tdestination') ):
              while ( have_rows('tdestination') ): the_row();
              $troomheading = get_sub_field('ht_destination_heading');
              $troomsubheading = get_sub_field('ht_destination_subheading');
          ?>

          <h2><?php echo $troomheading ?></h2>
          <p><?php echo $troomsubheading ?></p>

          <?php
            endwhile;
            else :
            // no rows found
            endif;
          ?>
        </div><!-- end .heading-section -->
      </div><!-- end .container -->
      <div class="fullwide">
        <div class="row">

          <?php
            // check if the repeater field has rows of data
            if( have_rows('attraction_list', 82) ) {
              $count = 1;

              while ( have_rows('attraction_list', 82) ) {

                the_row(); ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                  <div class="unibox unibox__frame transform__uno no-bradius">
                    <div class="uniboxinner">
                      <div class="unimages">
                        <img src="<?php the_sub_field('rl_image'); ?>" class="img-responsive" alt="<?php the_sub_field('rl_name'); ?>">
                      </div>
                      <div class="uninfo">
                        <h4><a href="<?php the_sub_field('rl_link'); ?>"><?php the_sub_field('rl_name'); ?></a></h4>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
                $count++;
                if ($count > 4) {
                  break;
                }
              }
            } ?>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <p class="text-center"><a href="<?php the_field('attraction_link_page'); ?>" class="btn btn-basic margintb-20">read more</a></p>
              </div>

        </div>
      </div><!-- end .fullwide -->
    </section>
  <?php else: ?>
    <!-- undisplay about us -->
  <?php endif; ?>

  <?php if( get_field('section_blog') ): ?>
    <section class="part-blog marginbot-40 padding-tbxlarge" data-aos="fade-up" data-aos-duration="1000">
      <div class="container">
        <div class="uniheading text-center marginbot-30">
          <?php
            if( have_rows('tblog') ):
              while ( have_rows('tblog') ): the_row();
              $tblogheading = get_sub_field('ht_blog_heading');
              $tblogsubheading = get_sub_field('ht_blog_subheading');
          ?>

          <h2><?php echo $tblogheading ?></h2>
          <p><?php echo $tblogsubheading ?></p>

          <?php
            endwhile;
            else :
            // no rows found
            endif;
          ?>
        </div><!-- end .heading-section -->
        <div class="row">
          <?php
            $post_objects = get_field('home_content_blog');

            if( $post_objects ): ?>

            <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
              <?php setup_postdata($post); ?>

              <?php
                // get image alt
                $thumb_id = get_post_thumbnail_id(get_the_id());
                $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                if(count($alt));
              ?>

              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="box-content blog-post unibox uniblog uniblog__duo transform marginbot-30">
                  <a href="<?php the_permalink(); ?>">
                    <div class="unibloginner">
                      <div class="unimages">
                        <?php
                          if ( has_post_thumbnail() ) {
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                            echo '<img src="'.$image[0].'" data-id="'.$post->ID.'" class="img-responsive" alt="'.$alt.'">';
                          }
                        ?>
                      </div>
                    </div><!-- end .box-image -->
                    <div class="box-info text-left">
                      <span class="time"><i class="ti-time" aria-hidden="true"></i> <?php the_time('F jS, Y'); ?></span>
                      <h2><?php the_title(); ?></h2>
                    </div><!-- end .box-info -->
                  </a>
                </div><!-- end .box-content -->
              </div><!-- end .col-md-3 col-sm-4 col-xs-12 -->
            <?php endforeach; ?>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
          <?php endif; ?>

          <div class="col-md-12 col-sm-12 col-xs-12">
            <p class="text-center"><a href="<?php the_field('blog_page_link'); ?>" class="btn btn-basic margintb-20">read more</a></p>
          </div>

        </div><!-- end .row -->
      </div><!-- end .container -->
    </section>
  <?php else: ?>
    <!-- undisplay blog -->
  <?php endif; ?>

</div><!-- end #content -->


<?php get_footer(); ?>
