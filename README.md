=== Mahitala ===
Contributors: Distrapps
Tags: clean, simple, company profile, hotel, resort, landing page
Requires at least: 4.8
Stable tag: v1.1.0
Version: 1.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Mahitala is a Wordpress Multipurpose Landing Page Theme. All Distrapps product adopt Easy to Use method (ESTU) for company or personal user and will user-friendly for visitor.

== Installation ==
This section describes how to install the child theme and get it working.

== Changelog ==
= 1.1.0 =
* Limit post on homepage

= 1.0.9 =
* Adjust homepage blog and list
* Tidy up style

= 1.0.8 =
* Add read more button
* Add favicon
* Tidy up style

= 1.0.7 =
* Adjust blog and destination page
* Tidy up style

= 1.0.6 =
* Update Checker

= 1.0.5 =
* Styling components
* Change tag

= 1.0.4 =
* Styling components
* Minify JS
* Add alt image
* Change tag

= 1.0.3 =
* Styling heading and header
* Add function for SEO support
* Change tag

= 1.0.2 =
* Styling font and footer
* Remove unused code and tag

= 1.0.1 =
* main components adjustment

= 1.0.0 =
* initial files
