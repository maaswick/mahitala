<?php

/**
 * Template Name: Contact
 */

get_header();

?>

<div id="content" class="page">

  <div class="page-intro">
    <?php if( get_field('cover_image') ): ?>
      <div class="intro-inner" style="background-image: url('<?php the_field('cover_image'); ?>')">
	  <?php endif; ?>
      <div class="outer-inner">
        <div class="inner-box clearfix">
          <div class="inner-box-container">
            <div class="intro-title">
              <h2><?php the_title(); ?> </h2>
              <p>Connecting outstanding people.</p>
            </div>
          </div><!-- end .inner-container -->
        </div><!-- end .inner-box -->
      </div><!-- end .outer-inner -->
    </div><!-- end .intro-inner -->
  </div><!-- end .page-intro -->

  <div class="content-wrap">
    <div class="heading-section heading-padspace text-center">
      <h2>Get in touch</h2>
      <p>The Experts Knows Better</p>
    </div><!-- end .heading-section -->

    <div class="container">
      <div class="row">
        <div class="col-md-7 col-sm-7 col-xs-12">
          <?php echo do_shortcode( '[contact-form-7 id="192" title="Contact form 1"]' ); ?>
        </div><!-- end .col-md-7 -->

        <div class="col-md-5 col-sm-5 col-xs-12">
          <ul class="noliststyle address-box">
            <li>
              <?php if(get_field('contact_info')): ?>
                <i class="icon fa fa-home"></i><?php the_field('contact_info'); ?>
              <?php endif; ?>
            </li>
            <li class="centered">
              <i class="icon fa fa-laptop"></i>
              <?php
              	// check if the repeater field has rows of data
              	if( have_rows('contact_email') ):

              		// loop through the rows of data
              		while ( have_rows('contact_email') ) : the_row(); ?>

                  <p><?php the_sub_field('email'); ?></p>

              <?php
              	endwhile;
            		else :
            			// no rows found
            		endif;
              ?>
            </li>
            <li>
              <i class="icon fa fa-user"></i>
              <?php

                // check if the repeater field has rows of data
                if( have_rows('contact_phone') ):

                  // loop through the rows of data
                  while ( have_rows('contact_phone') ) : the_row(); ?>

                  <p><?php the_sub_field('phone'); ?></p>

                <?php
                  endwhile;
                  else :
                    // no rows found
                  endif;
              ?>
            </li>
          </ul>
        </div><!-- end .col-md-5 -->
      </div><!-- end .row -->
    </div><!-- end .container -->

    <?php
      $location = get_field('contact_map');
      if( !empty($location) ):
    ?>
      <div class="acf-map">
        <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
      </div>
    <?php endif; ?>

  </div><!-- end .content-wrap -->

</div><!-- end .page -->

<?php get_footer(); ?>
