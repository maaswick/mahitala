
<footer class="main-footer">
  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="textwidget"><?php the_field('footer_copyright', 'option'); ?></div>
        </div>

        <div class="col-md-6">
          <div class="textwidget">
            <p><i class="ti-email"></i>&nbsp;<a href="mailto:grandsmilehoe@gmail.com?subject=feedback">&nbsp;Email us</a></p>
          </div>
        </div>

      </div>
    </div>
  </div><!-- end .copyright -->
</footer>

<?php wp_footer(); ?>

</body>
</html>
