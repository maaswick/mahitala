<?php

/**
 * Template Name: Blog
 */

get_header();

?>


<div id="content" class="page">

  <div class="content-wrap margintb-50">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
          <?php

          $post_objects = get_field('content_blog');

          if( $post_objects ): ?>
          <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
            <?php setup_postdata($post); ?>
          <div class="box-list">
            <?php
              if ( has_post_thumbnail() ) {
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                echo '<img src="'.$image[0].'" data-id="'.$post->ID.'" class="img-responsive">';
              }
            ?>

            <div class="meta">
              <span class="author"><i class="ti-user"></i>by <?php the_author(); ?></span>
              <span class="date"><i class="ti-calendar"></i><?php the_time('F jS, Y'); ?></span>
            </div>

              <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
              <?php echo the_excerpt(); ?>

            <a href="<?php the_permalink(); ?>" class="btn btn-basic">read more</a>
          </div><!-- end .box-list -->
        <?php endforeach; ?>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
      <?php endif; ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <aside class="sidebar sidebar-right">
            <div class="widget">
              <div class="widget blog-heading blog-recent-post">
                <h3 class="widget-title">Recent Post</h3>
                <ul>
                  <?php
                    $args = array( 'posts_per_page' => '3' );
                    $recent_posts = new WP_Query($args);
                    while( $recent_posts->have_posts() ) :
                        $recent_posts->the_post() ?>
                        <li>
                            <a href="<?php echo get_permalink() ?>"><?php the_title() ?></a>
                            <?php if ( has_post_thumbnail() ) : ?>
                                <?php the_post_thumbnail('thumbnail') ?>
                            <?php endif ?>
                            <?php echo '<span class="comment">' . date_i18n('d F Y', strtotime($recent['post_date'])) .'</span> '; ?>
                            <?php echo get_the_category_list( ', ', '', $recent["ID"] ); ?>
                        </li>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); # reset post data so that other queries/loops work
                  ?>
                </ul>
              </div><!-- end .widget -->
            </div><!-- end .widget -->
          </aside>
        </div><!-- end .col-md-4 -->
      </div>
    </div>
  </div><!-- end .content-wrap -->


</div>


<?php get_footer(); ?>
