<?php

/**
 * Template Name: Attraction
 */

get_header();

?>

<div id="content" class="page">

  <div class="page-intro">
    <?php if( get_field('attraction_list_cover') ): ?>
      <div class="intro-inner" style="background-image: url('<?php the_field('attraction_list_cover'); ?>')">
	  <?php endif; ?>
      <div class="outer-inner">
        <div class="inner-box clearfix">
          <div class="inner-box-container">
            <div class="intro-title">
              <h1><?php the_title(); ?> </h1>
            </div>
          </div><!-- end .inner-container -->
        </div><!-- end .inner-box -->
      </div><!-- end .outer-inner -->
    </div><!-- end .intro-inner -->
  </div><!-- end .page-intro -->

  <div class="content-wrap margintb-50">
    <section class="section-introduce section-attraction">
      <div class="container">
        <div class="row">
          <?php

        		// check if the repeater field has rows of data
        		if( have_rows('attraction_list') ):

        			// loop through the rows of data
        			while ( have_rows('attraction_list') ) : the_row(); ?>

              <div class="col-md-3 col-sm-3 col-xs-6" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="50">
                <div class="csunibox csunibox__ro margin-blarge">
                  <div class="csunipic csunipic--ro">
                    <span class="unimage">
                      <img src="<?php the_sub_field('rl_image'); ?>" class="img-responsive" alt="<?php the_sub_field('rl_name'); ?>">
                    </span>
                  </div>
                  <div class="csoverlay">
                    <div class="csovertitle margintb-10">
                      <h4><a href="<?php the_sub_field('rl_link'); ?>"><?php the_sub_field('rl_name'); ?></a></h4>
                    </div>
                  </div>
                </div><!-- end .csunibox -->
              </div>

            <?php
              endwhile;

            else :

              // no rows found

            endif;

          ?>
        </div><!-- end .row -->

      </div><!-- end .container -->
    </section><!-- end .section-introduce -->
  </div><!-- end .content-wrap -->
</div>


<?php get_footer(); ?>
