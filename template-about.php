<?php

/**
 * Template Name: About
 */

get_header();

?>


<div id="content" class="page">

  <div class="content-wrap margintb-50">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <?php the_field('pa_description'); ?>
        </div>
      </div>
    </div>
  </div><!-- end .content-wrap -->

</div>

<?php get_footer(); ?>
