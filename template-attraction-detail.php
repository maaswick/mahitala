<?php

/**
 * Template Name: Attraction Details
 */

get_header();

?>

<div id="content" class="page">

  <div class="page-intro">
    <?php if( get_field('attraction_cover_image') ): ?>
      <div class="intro-inner" style="background-image: url('<?php the_field('attraction_cover_image'); ?>')">
	  <?php endif; ?>
    </div><!-- end .intro-inner -->
  </div><!-- end .page-intro -->

  <div class="content-wrap">
    <section class="section-introduce">
      <div class="container">
        <div class="uniheading heading-padspace text-center">
            <h1><?php the_title(); ?></h1>
            <?php the_field('attraction_intro'); ?>
        </div>

        <div class="row">

          <?php
            $images = get_field('attraction_images');
            if( $images ): ?>

              <div id="slider-main" class="owl-carousel">
                <?php foreach( $images as $image ): ?>
                  <div class="owl-slide">
                    <div class="image-slide" style="background-image: url('<?php echo $image['url']; ?>')"></div>
                  </div>
                <?php endforeach; ?>
              </div><!-- end .slider-main -->

          <?php endif; ?>

        </div><!-- end .row -->
      </div><!-- end .container -->
    </section><!-- end .section-introduce -->

    <section class="section-description">
      <div class="container">
        <div class="padding-40">
          <?php the_field('attraction_description'); ?>
        </div>
      </div><!-- end .container -->
    </section><!-- end .section-description -->
  </div><!-- end .content-wrap -->

</div><!-- end #content -->

<?php get_footer(); ?>
